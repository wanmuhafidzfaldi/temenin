package database

import (
	"database/sql"
	_"github.com/go-sql-driver/mysql"
	"log"
)

func ConnectMysqlDB() *sql.DB {
	//db, err := sql.Open("mysql", "tele-db:(Tele-db123!!)@tcp(localhost:3306)/Telemedicine")
	db, err := sql.Open("mysql", "tele-db:(Tele-db123!!)@tcp(localhost:3306)/Telemedicine_ujicoba")
	//db, err := sql.Open("mysql", "root:@tcp(localhost:3306)/Telemedicine")

	if err != nil {
		log.Fatal(err)
	}

	return db
}
