package config

import (
	"github.com/kataras/iris"
	"temenin/resource/dokter"
	"temenin/resource/pasien"
	"temenin/resource/tele"
	"temenin/resource/notifikasi"
	"temenin/auth"
	"github.com/kataras/iris/core/router"
	"temenin/resource/tele_expertise"
	"temenin/resource/rumah_sakit"
	"temenin/resource/chat_notifikasi"
)

func Router() *iris.Application  {
	app := iris.New()

	app.Use(auth.AuthApiKey)
	app.Handle("GET", "/", func(ctx iris.Context) {
		type Response struct {
			Status  int    `json:"status"`
			Message string `json:"message"`
		}
		var respone Response
		respone.Status = 1
		respone.Message = "Authorize"
		ctx.JSON(respone)
	})
	app.Post("/api/v1/mobile/login", auth.LoginGetToken)

	app.PartyFunc("/api/v1/mobile", func(p iris.Party) {
		p.Use(auth.AuthToken)
		p.PartyFunc("/dokter", func(p2 iris.Party) {
			p2.Get("/", dokter.Show)
			p2.Get("/{id}", dokter.ShowById)
			p2.Get("/nama/{nama_dokter}", dokter.ShowByName)
		})
		p.PartyFunc("/rumah_sakit", func(p2 router.Party) {
			p2.Get("/{id_rs}", rumah_sakit.ShowById)
		})
		p.PartyFunc("/pasien", func(p2 iris.Party) {
			p2.Get("/", pasien.Show)
			p2.Get("/{id}", pasien.ShowById)
			p2.Get("/rs/{id_rs}", pasien.ShowById)
			p2.Get("/search_name/{nama_pasien}", pasien.ShowById)
			p2.Post("/", pasien.Create)
		})
		p.PartyFunc("/tele", func(p2 iris.Party) {
			p2.Use(auth.IsDokter)
			p2.Get("/", tele.Show)
			p2.Post("/", tele.Create)
			p2.Get("/{id_tele}", tele.ShowById)
			p2.Patch("/{id_tele}", tele.Update)
		})
		p.PartyFunc("/expertise", func(p2 router.Party) {
			p2.Use(auth.IsDokter)
			p2.Get("/dokter/{id_dokter}/{peng_di_rs}/{jenis_tele}", tele_expertise.Show)
			p2.Get("/rs/{id_rs}/all", tele_expertise.ShowDetailAll)
			p2.Post("/", tele_expertise.Create)
			p2.Get("/{id_tele}", tele_expertise.ShowById)
			p2.Patch("/{id_tele}", tele_expertise.Update)
		})
		p.PartyFunc("/chat-notifikasi", func(p2 router.Party) {
			p2.Use(auth.IsDokter)
			p2.Get("/{id_penerima}", chat_notifikasi.ShowAllById)
			p2.Post("/", chat_notifikasi.Create)
		})

		p.PartyFunc("/notifikasi", func(p2 iris.Party) {
			p2.Get("/{status}/{id_rs}/{peng_di_rs}/{spesialis}", notifikasi.Show)
			p2.Get("/{status}/{id_rs}/{peng_di_rs}", notifikasi.Show)
		})
		p.Get("/", func(ctx iris.Context) {
			type Response struct {
				Status  int    `json:"status"`
				Message string `json:"message"`
			}
			var respone Response
			respone.Status = 1
			respone.Message = "Authorize"
			ctx.JSON(respone)
		})
	})
	return app
}
