package auth

import (
	"github.com/kataras/iris"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"temenin/config/database"
	"log"
	"strings"
)

type Response struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Token   string `json:"token"`
}

type ResponseLogin struct {
	Status      int `json:"status"`
	Message     string `json:"message"`
	Token       string `json:"token"`
	Id_user     string `json:"idUser"`
	Id_rs       string `json:"idRs"`
	Username    string `json:"username"`
	Nama        string `json:"nama"`
	Status_user string `json:"statusUser"`
	Peng_di_rs  string `json:"pengDiRs"`
	Spesialis   string `json:"spesialis"`
	Foto_user   string `json:"fotoUser"`
}

func AuthApiKey(ctx iris.Context) {
	var respone Response
	authorizationHeader := ctx.GetHeader("x-api-key")
	if (authorizationHeader == "On4MgTXPOJoXtlMSeuPu5FSTLnrLuBhF") {
		respone.Status = 1
		respone.Message = "Authorize"
		ctx.Next()
	} else {
		respone.Status = 0
		respone.Message = "Unauthorize"
		ctx.JSON(respone)
	}
}
func AuthToken(ctx iris.Context)  {
	var response Response
	id_user := ""
	username := ""
	status := ""
	device_id := ""
	authorizationToken := ctx.GetHeader("token")
	if (authorizationToken != "") {
		bearerToken := strings.Split(authorizationToken, ".")
		//fmt.Println(len(bearerToken))
		if (len(bearerToken) == 3) {
			token, err := jwt.Parse(authorizationToken, func(token *jwt.Token) (interface{}, error) {
				var secret = ""
				// Don't forget to validate the alg is what you expect:
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
				}
				if claims, ok := token.Claims.(jwt.MapClaims); ok {
					db := database.ConnectMysqlDB()
					defer db.Close()
					status = claims["status"].(string)
					username = claims["username"].(string)
					id_user = claims["id_user"].(string)
					if (status == "dokter") {
						rows, err := db.Query("select device_id from dokter where username_dktr = '" + username + "' and id_dktr = '" + id_user + "'")
						if err != nil {
							log.Print(err)
						}
						for rows.Next() {
							if err := rows.Scan(&device_id); err != nil {
								//log.Fatal(err.Error())
								response.Status = 0
								response.Message = "Gagal Data Dokter"
								ctx.JSON(response)
							} else {
								secret = device_id
							}
						}
					} else {
						if (status == "operator") {
							rows, err := db.Query("select device_id from user_rs where username = '" + username + "' and id_user = '" + id_user + "'")
							if err != nil {
								log.Print(err)
							}
							for rows.Next() {
								if err := rows.Scan(&device_id); err != nil {
									log.Fatal(err.Error())
									response.Status = 0
									response.Message = "Gagal Data Operator"
									ctx.JSON(response)
								} else {
									secret = device_id
								}
							}
						}
					}

				} else {
					fmt.Println("Claim Not Unverify")
				}
				// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
				return []byte(secret), nil
			})

			if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
				fmt.Println(claims["id_user"], claims["status"], claims["username"])
				ctx.Next()
			} else {
				fmt.Println(err)
				response.Status = 0
				response.Message = err.Error()
				ctx.JSON(response)
				return
			}

		}
	} else {
		response.Status = 0
		response.Message = "An authorization header is required"
		ctx.JSON(response)
		return
	}
}

func IsDokter(ctx iris.Context){
	var response Response
	if (ctx.GetHeader("status") == "dokter"){
		ctx.Next()
	} else {
		response.Status = 0
		response.Message = "Not authorization"
		ctx.JSON(response)
	}
}

func LoginGetToken(ctx iris.Context) {
	var responseLogin ResponseLogin
	if ctx.Method() != "POST" {
		log.Fatal(ctx, "No POST", ctx.Method())
		return
	}

	username := ctx.FormValue("username")
	password := ctx.FormValue("password")
	status := ctx.FormValue("status")
	device_id := ctx.FormValue("device_id")
	firebase_token := ctx.FormValue("firebase_token")

	if (username == "" || password == "" || status == "") {
		ctx.JSON(iris.Map{"status": 0, "message": "username or password is empty"})
		return
	}
	db := database.ConnectMysqlDB()
	defer db.Close()
	if (status == "dokter") {
		rows, err := db.Query("select id_dktr, dokter.id_rs, username_dktr, nama_dktr, foto_dktr, spesialis_dktr, rumah_sakit.peng_di_rs from dokter join rumah_sakit on dokter.id_rs = rumah_sakit.id_rs  where username_dktr = '" + username + "' and password_dktr = '" + password + "'")
		if err != nil {
			log.Print(err)
		}
		for rows.Next() {
			if err := rows.Scan(&responseLogin.Id_user, &responseLogin.Id_rs, &responseLogin.Username, &responseLogin.Nama, &responseLogin.Foto_user, &responseLogin.Spesialis, &responseLogin.Peng_di_rs); err != nil {
				log.Fatal(err.Error())
				responseLogin.Status = 0
				responseLogin.Message = "Username & Password Anda Tidak Terdaftar"
				ctx.JSON(responseLogin)
				return
			} else {
				stmt, err := db.Prepare("update dokter set device_id = ?, firebase_token = ? where id_dktr = ?")
				checkErr(err)
				if _, err := stmt.Exec(device_id, firebase_token, responseLogin.Id_user); err != nil {
					checkErr(err)
				} else {
					token := jwt.New(jwt.SigningMethodHS512)
					token.Claims = jwt.MapClaims{
						"id_user":  responseLogin.Id_user,
						"username": responseLogin.Username,
						"status":   "dokter",
					}
					tokenString, error := token.SignedString([]byte(device_id))
					if error != nil {
						fmt.Println(error)
					}
					responseLogin.Status = 1
					responseLogin.Message = "Berhasil Login"
					responseLogin.Token = tokenString
					responseLogin.Status_user = "dokter"
					ctx.JSON(responseLogin)
					return
				}
			}
		}
	} else if (status == "operator") {
		rows, err := db.Query("select id_user, user_rs.id_rs, username, nama, rumah_sakit.peng_di_rs  from user_rs join rumah_sakit on rumah_sakit.id_rs = user_rs.id_rs where username = '" + username + "' and password = '" + password + "'")
		if err != nil {
			log.Print(err)
		}
		for rows.Next() {
			if err := rows.Scan(&responseLogin.Id_user, &responseLogin.Id_rs, &responseLogin.Username, &responseLogin.Nama, &responseLogin.Peng_di_rs); err != nil {
				log.Fatal(err.Error())
				responseLogin.Status = 0
				responseLogin.Message = "Username & Password Anda Tidak Terdaftar"
				ctx.JSON(responseLogin)
				return
			} else {

				stmt, err := db.Prepare("update user_rs set device_id = ?, firebase_token = ? where id_user = ?")
				checkErr(err)

				if _, err := stmt.Exec(device_id, firebase_token, responseLogin.Id_user); err != nil {
					checkErr(err)
				} else {
					token := jwt.New(jwt.SigningMethodHS512)
					token.Claims = jwt.MapClaims{
						"id_user":  responseLogin.Id_user,
						"username": responseLogin.Username,
						"status":   "operator",
					}
					tokenString, error := token.SignedString([]byte(device_id))
					if error != nil {
						fmt.Println(error)
					}
					responseLogin.Status = 1
					responseLogin.Message = "Berhasil Login"
					responseLogin.Token = tokenString
					responseLogin.Status_user = "operator"
					responseLogin.Foto_user = "user.png"
					ctx.JSON(responseLogin)
					return
				}

			}
		}
	}
	ctx.JSON(iris.Map{"message": "error get token", "status": 0})
	return
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
