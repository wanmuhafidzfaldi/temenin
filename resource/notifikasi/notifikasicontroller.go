package notifikasi

import "github.com/kataras/iris"

func Show(ctx iris.Context)  {
	type Response struct {
		Status int `json:"status"`
		Message string `json:"message"`
		Notifikasi []Notifikasi `json:"notifikasi"`
	}
	var response Response
	idRs := ctx.Params().Get("id_rs")
	spesialis := ctx.Params().Get("spesialis")
	status := ctx.Params().Get("status")
	pengDiRs := ctx.Params().Get("peng_di_rs")
	if (status == "operator"){
		status = "operator"
		response.Status = 1;
		response.Message = "Result Data"
		if (pengDiRs == "p2" || pengDiRs == "p1"){
			response.Notifikasi = GetAllOperatorPengampu(idRs, spesialis, status, pengDiRs)
		} else if (pengDiRs == "d1" || pengDiRs == "d2") {
			response.Notifikasi = GetAllOperatorDiampu(idRs, spesialis, status, pengDiRs)
		}
		if (len(response.Notifikasi) == 0){
			response.Status = 0;
			response.Message = "Tidak Ada Data Notifikasi"
		}
	} else if (status == "dokter"){
		status = "dokter"
		response.Status = 1;
		response.Message = "Result Data"
		if (spesialis != "dr") {
			response.Notifikasi = GetAllDokterPengampu(idRs, spesialis, status, pengDiRs)
		} else {
			response.Notifikasi = GetAllDokterDiampu(idRs, spesialis, status, pengDiRs)
		}

		if (len(response.Notifikasi) == 0){
			response.Status = 0;
			response.Message = "Tidak Ada Data Notifikasi"
		}
	}
	ctx.JSON(response)
}
