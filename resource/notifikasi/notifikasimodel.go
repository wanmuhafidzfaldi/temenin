package notifikasi

import (
	"log"
	"strings"
	"temenin/config/database"
)

func GetAllOperatorPengampu(idRs string, spesialis string, status string, pengDiRs string) []Notifikasi {
	var notifikasi Notifikasi
	result := []Notifikasi{}
	db := database.ConnectMysqlDB()
	defer db.Close()
	rows, err := db.Query("SELECT tele.id_tele, tele.jenis_tele ,pasien.nama_pasien, rumah_sakit.nama_rs, tele.tgl_foto FROM tele, riwayat_pasien, pasien,rumah_sakit WHERE pasien.id_pasien = riwayat_pasien.id_pasien and rumah_sakit.id_rs = pasien.id_rs_diampu and riwayat_pasien.id_riwayat = tele.id_tele and tele.id_rs like '%"+idRs+"%' and tele.id_tele  not in (SELECT tele_expertise.id_tele from tele_expertise)")
	if err != nil {
		log.Print(err)
	}
	for rows.Next() {
		if err := rows.Scan(&notifikasi.IdTele, &notifikasi.JenisTele, &notifikasi.NamaPasien, &notifikasi.NamaRsDiampu, &notifikasi.TglFoto); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {
			result = append(result, notifikasi)
		}
	}
	return result
}

func GetAllOperatorDiampu(idRs string, spesialis string, status string, pengDiRs string) []Notifikasi {
	var notifikasi Notifikasi
	result := []Notifikasi{}
	db := database.ConnectMysqlDB()
	defer db.Close()
	rows, err := db.Query("SELECT tele.id_tele, tele.jenis_tele ,pasien.nama_pasien, rumah_sakit.nama_rs, tele.tgl_foto FROM tele,riwayat_pasien,pasien,rumah_sakit WHERE pasien.id_pasien = riwayat_pasien.id_pasien and rumah_sakit.id_rs = pasien.id_rs_diampu and riwayat_pasien.id_riwayat = tele.id_tele and pasien.id_rs_diampu = '"+idRs+"' and tele.id_tele in (SELECT tele_expertise.id_tele from tele_expertise where baca ='0')")
	if err != nil {
		log.Print(err)
	}
	for rows.Next() {
		if err := rows.Scan(&notifikasi.IdTele, &notifikasi.JenisTele, &notifikasi.NamaPasien, &notifikasi.NamaRsDiampu, &notifikasi.TglFoto); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {
			result = append(result, notifikasi)
		}
	}
	return result
}

func GetAllDokterPengampu(idRs string, spesialis string, status string, pengDiRs string) []Notifikasi  {
	var notifikasi Notifikasi
	result := []Notifikasi{}
	db := database.ConnectMysqlDB()
	defer db.Close()
	rows, err := db.Query("SELECT tele.id_tele, tele.jenis_tele ,pasien.nama_pasien, rumah_sakit.nama_rs, tele.tgl_foto FROM tele, riwayat_pasien, pasien,rumah_sakit WHERE pasien.id_pasien = riwayat_pasien.id_pasien and rumah_sakit.id_rs = pasien.id_rs_diampu and riwayat_pasien.id_riwayat = tele.id_tele and tele.jenis_tele = '" + strings.ToUpper(spesialis) + "' and tele.id_rs like '%" + idRs + "%' and tele.id_tele  not in (SELECT tele_expertise.id_tele from tele_expertise)")
	if err != nil {
		log.Print(err)
	}
	for rows.Next() {
		if err := rows.Scan(&notifikasi.IdTele, &notifikasi.JenisTele, &notifikasi.NamaPasien, &notifikasi.NamaRsDiampu, &notifikasi.TglFoto); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {
			result = append(result, notifikasi)
		}
	}
	return result
}

func GetAllDokterDiampu(idRs string, spesialis string, status string, pengDiRs string) []Notifikasi  {
	var notifikasi Notifikasi
	result := []Notifikasi{}
	db := database.ConnectMysqlDB()
	defer db.Close()
	rows, err := db.Query("SELECT tele.id_tele, tele.jenis_tele ,pasien.nama_pasien, rumah_sakit.nama_rs, tele.tgl_foto FROM tele,riwayat_pasien,pasien,rumah_sakit WHERE pasien.id_pasien = riwayat_pasien.id_pasien and rumah_sakit.id_rs = pasien.id_rs_diampu and riwayat_pasien.id_riwayat = tele.id_tele and pasien.id_rs_diampu = '"+idRs+"' and tele.id_tele in (SELECT tele_expertise.id_tele from tele_expertise where baca ='0')")
	if err != nil {
		log.Print(err)
	}
	for rows.Next() {
		if err := rows.Scan(&notifikasi.IdTele, &notifikasi.JenisTele, &notifikasi.NamaPasien, &notifikasi.NamaRsDiampu, &notifikasi.TglFoto); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {
			result = append(result, notifikasi)
		}
	}
	return result
}


