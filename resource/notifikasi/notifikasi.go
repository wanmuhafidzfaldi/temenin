package notifikasi

type Notifikasi struct {
	IdTele       string `json:"idTele"`
	JenisTele    string `json:"jenisTele"`
	NamaPasien   string `json:"namaPasien"`
	NamaRsDiampu string `json:"namaRsDiampu"`
	TglFoto      string `json:"tglFoto"`
}
