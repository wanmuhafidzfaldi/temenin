package tele_expertise

import (
	"github.com/kataras/iris"
	"time"
	"strings"
)

func Show(ctx iris.Context)  {
	if(ctx.Params().Get("id_dokter") != ""){
		if(ctx.Params().Get("peng_di_rs") == "d1" || ctx.Params().Get("peng_di_rs") == "d2") {
			type Response struct {
				Status				int					`json:"status"`
				Message				string              `json:"message"`
				TeleExpertiseList	[]TeleExpertiseList   `json:"teleExpertiseList"`
			}
			var response Response
			response.TeleExpertiseList = GetListExpertiseDiampu(ctx.Params().Get("id_dokter"), strings.ToUpper(ctx.Params().Get("jenis_tele")))
			if (len(response.TeleExpertiseList) != 0) {
				response.Status = 1
				response.Message = "Result Data"
				ctx.JSON(response)
			} else {
				response.Status = 0
				response.Message = "Error Data"
				ctx.JSON(response)
			}
		} else if(ctx.Params().Get("peng_di_rs") == "p1" || ctx.Params().Get("peng_di_rs") == "p2") {
			type Response struct {
				Status				int					`json:"status"`
				Message				string              `json:"message"`
				TeleExpertiseList	[]TeleExpertiseList   `json:"teleExpertiseList"`
			}
			var response Response
			response.TeleExpertiseList = GetListExpertisePengampu(ctx.Params().Get("id_dokter"))
			if (len(response.TeleExpertiseList) != 0) {
				response.Status = 1
				response.Message = "Result Data"
				ctx.JSON(response)
			} else {
				response.Status = 0
				response.Message = "Error Data"
				ctx.JSON(response)
			}
		}
	}
}

func ShowDetailAll(ctx iris.Context)  {

}

func ShowById(ctx iris.Context) {
	type Response struct {
		Status        int           `json:"status"`
		Message       string        `json:"message"`
		TeleExpertise TeleExpertise `json:"teleExpertise"`
	}
	idTele := ctx.Params().Get("id_tele")
	var response Response
	response.TeleExpertise = GetByIdTele(idTele)
	if (response.TeleExpertise.Id_expertise == "") {
		response.Status = 0
		response.Message = "Error Data"
		ctx.JSON(response)
	} else {
		response.Status = 1
		response.Message = "Result Data"
		ctx.JSON(response)
	}

}

func Update(ctx iris.Context){
	type Response struct {
		Status  int    `json:"status"`
		Message string `json:"message"`
	}
	var response Response
	var teleExpertise TeleExpertise
	var idTele = ctx.Params().GetString("id_tele")

	teleExpertise.Id_tele = idTele
	teleExpertise.Hasil_expertise = ctx.FormValue("hasil_expertise")
	teleExpertise.Saran = ctx.FormValue("saran")
	teleExpertise.Kondisi_pasien = ctx.FormValue("kondisi_pasien")
	teleExpertise.Id_dokter = ctx.FormValue("id_dokter")
	teleExpertise.Tgl_expertise = ctx.FormValue("tgl_expertise")
	teleExpertise.Baca = ctx.FormValue("baca")

	if (UpdateById(idTele, teleExpertise) != 0) {
		response.Status = 1
		response.Message = "Data Berhasil Diupdate"
		ctx.JSON(response)
	} else {
		response.Status = 0
		response.Message = "Data Gagal Diupdate"
		ctx.JSON(response)
	}
}
func Create(ctx iris.Context) {
	type Response struct {
		Status  int    `json:"status"`
		Message string `json:"message"`
	}
	var response Response
	var teleExpertise TeleExpertise

	teleExpertise.Id_tele = ctx.FormValue("id_tele")
	teleExpertise.Hasil_expertise = ctx.FormValue("hasil_expertise")
	teleExpertise.Saran = ctx.FormValue("saran")
	teleExpertise.Kondisi_pasien = ctx.FormValue("kondisi_pasien")
	teleExpertise.Id_dokter = ctx.FormValue("id_dokter")
	teleExpertise.Tgl_expertise = time.Now().Local().Format("2006-01-02 15:04:05")
	teleExpertise.Baca = "0"

	if (Insert(teleExpertise) != 0) {
		response.Status = 1
		response.Message = "Data Berhasil Diinput"
		ctx.JSON(response)
	} else {
		response.Status = 0
		response.Message = "Data Gagal Diinput"
		ctx.JSON(response)
	}

}
