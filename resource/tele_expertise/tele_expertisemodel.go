package tele_expertise

import (
	"temenin/config/database"
	"log"
)

func GetListExpertiseDiampu(idDokter string, jenisTele string) []TeleExpertiseList{
	var teleExpertiseList TeleExpertiseList
	result := []TeleExpertiseList{}
	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select tele_expertise.id_tele, tele.jenis_tele, pasien.nama_pasien, rumah_sakit.nama_rs, tele_expertise.tgl_expertise from tele_expertise left join tele on tele.id_tele = tele_expertise.id_tele left join riwayat_pasien on riwayat_pasien.id_tele = tele.id_tele left join pasien on pasien.id_pasien = riwayat_pasien.id_pasien left join dokter on dokter.id_dktr = tele_expertise.id_dokter left join rumah_sakit on rumah_sakit.id_rs = dokter.id_rs where riwayat_pasien.id_dokter = '"+idDokter+"' and tele.jenis_tele = '"+jenisTele+"'")
	checkErr(err)
	for rows.Next() {
		if err := rows.Scan(&teleExpertiseList.IdTele, &teleExpertiseList.JenisTele, &teleExpertiseList.NamaPasien, &teleExpertiseList.NamaRs, &teleExpertiseList.TglExpertise); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {
			result = append(result,teleExpertiseList)
		}
	}
	return result
}

func GetListExpertisePengampu(idDokter string) []TeleExpertiseList{
	var teleExpertiseList TeleExpertiseList
	result := []TeleExpertiseList{}
	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select tele_expertise.id_tele, tele.jenis_tele, pasien.nama_pasien, rumah_sakit.nama_rs, tele_expertise.tgl_expertise from tele_expertise left join tele on tele.id_tele = tele_expertise.id_tele left join riwayat_pasien on riwayat_pasien.id_tele = tele.id_tele left join pasien on pasien.id_pasien = riwayat_pasien.id_pasien left join dokter on dokter.id_dktr = tele_expertise.id_dokter left join rumah_sakit on rumah_sakit.id_rs = dokter.id_rs where tele_expertise.id_dokter = '"+idDokter+"'")
	checkErr(err)
	for rows.Next() {
		if err := rows.Scan(&teleExpertiseList.IdTele, &teleExpertiseList.JenisTele, &teleExpertiseList.NamaPasien, &teleExpertiseList.NamaRs, &teleExpertiseList.TglExpertise); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {
			result = append(result,teleExpertiseList)
		}
	}
	return result
}

func GetByIdTele(idTele string)  TeleExpertise {
	var teleExpertise TeleExpertise
	result := TeleExpertise{}
	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_expertise, id_tele, hasil_expertise, saran, kondisi_pasien, tgl_expertise, id_dokter, baca from tele_expertise where id_tele = '"+idTele+"'")
	checkErr(err)
	for rows.Next() {
		if err := rows.Scan(&teleExpertise.Id_expertise, &teleExpertise.Id_tele, &teleExpertise.Hasil_expertise, &teleExpertise.Saran, &teleExpertise.Kondisi_pasien, &teleExpertise.Tgl_expertise, &teleExpertise.Id_dokter, &teleExpertise.Baca); err != nil {
			log.Fatal(err.Error())
			return teleExpertise
		} else {
			result = teleExpertise
		}
	}
	return result
}

func UpdateById(idTele string, teleExpertise TeleExpertise) int {
	db := database.ConnectMysqlDB()
	defer  db.Close()
	stmt, err := db.Prepare("update tele_expertise set hasil_expertise = ?, saran = ?, kondisi_pasien = ?, tgl_expertise = ?, id_dokter = ?, baca = ? where id_tele = ?")
	checkErr(err)
	if err != nil {
		log.Println(err)
	}

	if _, err := stmt.Exec(&teleExpertise.Hasil_expertise, &teleExpertise.Saran, &teleExpertise.Kondisi_pasien, &teleExpertise.Tgl_expertise, &teleExpertise.Id_dokter, &teleExpertise.Baca, &idTele); err != nil {
		checkErr(err)
		return 0
	} else {
		return 1
	}
	return 0
}

func Insert(teleExpertise TeleExpertise) int  {
	db := database.ConnectMysqlDB()
	defer  db.Close()
	var jlh int
	rows, err := db.Query("select count(*) from tele_expertise where tele_expertise.id_tele = '" + teleExpertise.Id_tele+ "'")
	checkErr(err)
	for rows.Next() {
		if err := rows.Scan(&jlh); err != nil {
			log.Fatal(err.Error())
			return jlh
		} else {
			if (jlh == 0){
				stmt, err := db.Prepare("insert into tele_expertise value (?, ?, ?, ?, ?, ?, ?, ?)")
				checkErr(err)
				if err != nil {
					log.Println(err)
				}
				if res, err := stmt.Exec("null", teleExpertise.Id_tele, teleExpertise.Hasil_expertise, teleExpertise.Saran, teleExpertise.Kondisi_pasien, teleExpertise.Tgl_expertise, teleExpertise.Id_dokter, teleExpertise.Baca); err != nil {
					checkErr(err)
					return 0
				} else {
					lastId, _ := res.LastInsertId()
					return int(lastId)
				}
			}
		}
	}

	return 0
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
