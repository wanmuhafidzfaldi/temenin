package tele_expertise

type TeleExpertise struct {
	Id_expertise    string `json:"idExpertise"`
	Id_tele         string `json:"idTele"`
	Hasil_expertise string `json:"hasilExpertise"`
	Saran           string `json:"saran"`
	Kondisi_pasien  string `json:"kondisiPasien"`
	Tgl_expertise   string `json:"tglExpertise"`
	Id_dokter       string `json:"idDokter"`
	Baca            string `json:"baca"`
}

type TeleExpertiseList struct {
	IdTele       string `json:"idTele"`
	JenisTele    string `json:"jenisTele"`
	NamaPasien   string `json:"namaPasien"`
	NamaRs		 string `json:"namaRs"`
	TglExpertise string `json:"tglExpertise"`
}
