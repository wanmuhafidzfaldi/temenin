package chat_notifikasi

import "github.com/kataras/iris"

func Create(ctx iris.Context)  {
	type Response struct {
		Status int                   	`json:"status"`
		Message string               	`json:"message"`
		ChatNotifikasi ChatNotifikasi	`json:"chatNotifikasi"`
	}
	var response Response
	idDokterPengirim := ctx.FormValue("id_dokter_pengirim")
	idDokterPenerima := ctx.FormValue("id_dokter_penerima")
	status := ctx.FormValue("status")
	response.ChatNotifikasi = GetByPengirimPenerima(idDokterPengirim, idDokterPenerima)
	if (response.ChatNotifikasi.Id_chat_notifikasi == ""){
		response.ChatNotifikasi.Id_dokter_pengirim = idDokterPengirim
		response.ChatNotifikasi.Id_dokter_penerima = idDokterPenerima
		response.ChatNotifikasi.Status = status
		if (Insert(response.ChatNotifikasi) != 0){
			response.Status = 1
			response.Message = "Result Data"
			ctx.JSON(response)
		} else {
			response.Status = 0
			response.Message = "Error Data"
			ctx.JSON(response)
		}
	} else {
		response.ChatNotifikasi.Id_dokter_pengirim = idDokterPengirim
		response.ChatNotifikasi.Id_dokter_penerima = idDokterPenerima
		response.ChatNotifikasi.Status = status
		if (Update(response.ChatNotifikasi) == 1){
			response.Status = 1
			response.Message = "Result Data"
			ctx.JSON(response)
		}else {
			response.Status = 0
			response.Message = "Error Data"
			ctx.JSON(response)
		}
	}

}

func ShowAllById(ctx iris.Context) {
	type Response struct {
		Status int `json:"status"`
		Message string `json:"message"`
		ChatNotifikasi []ChatNotifikasi `json:"chatNotifikasi"`
	}
	var response Response
	response.ChatNotifikasi = GetByIdPenerima(ctx.Params().Get("id_penerima"))
	if (len(response.ChatNotifikasi) != 0){
		response.Status = 1
		response.Message = "Result Data"
		ctx.JSON(response)
	} else {
		response.Status = 0
		response.Message = "Error Data"
		ctx.JSON(response)
	}
}
