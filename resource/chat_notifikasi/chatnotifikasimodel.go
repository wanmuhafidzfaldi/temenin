package chat_notifikasi

import (
	"temenin/config/database"
	"log"
)

func GetByIdPenerima(idDokterPenerima string) []ChatNotifikasi {
	var chatNotifikasi ChatNotifikasi
	result := []ChatNotifikasi{}

	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_chat_notifikasi, id_dokter_pengirim, id_dokter_penerima, status from chat_notifikasi where id_dokter_penerima = '" + idDokterPenerima + "' and status = '1'")
	if (err != nil) {
		log.Print(err)
	}

	for rows.Next() {
		if err := rows.Scan(&chatNotifikasi.Id_chat_notifikasi, &chatNotifikasi.Id_dokter_pengirim, &chatNotifikasi.Id_dokter_penerima, &chatNotifikasi.Status); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {
			result = append(result, chatNotifikasi)
		}
	}
	return result
}

func GetByPengirimPenerima(idDokterPengirim string, idDokterPenerima string) ChatNotifikasi {
	var chatNotifikasi ChatNotifikasi
	result := ChatNotifikasi{}

	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_chat_notifikasi, id_dokter_pengirim, id_dokter_penerima, status from chat_notifikasi where id_dokter_pengirim = '"+idDokterPengirim+"' and id_dokter_penerima = '" + idDokterPenerima + "'")
	if (err != nil) {
		log.Print(err)
	}

	for rows.Next() {
		if err := rows.Scan(&chatNotifikasi.Id_chat_notifikasi, &chatNotifikasi.Id_dokter_pengirim, &chatNotifikasi.Id_dokter_penerima, &chatNotifikasi.Status); err != nil {
			log.Fatal(err.Error())
			return chatNotifikasi
		} else {
			result = chatNotifikasi
		}
	}
	return result
}

func Update(chatNotifikasi ChatNotifikasi) int {
	db := database.ConnectMysqlDB()
	defer  db.Close()
	stmt, err := db.Prepare("update chat_notifikasi set status = ? where id_dokter_pengirim = ? and id_dokter_penerima = ?")
	checkErr(err)
	if err != nil {
		log.Println(err)
	}

	if _, err := stmt.Exec(chatNotifikasi.Status, chatNotifikasi.Id_dokter_pengirim, chatNotifikasi.Id_dokter_penerima); err != nil {
		checkErr(err)
		return 0
	} else {
		return 1
	}
	return 0
}

func Insert(chatNotifikasi ChatNotifikasi) int{
	db := database.ConnectMysqlDB()
	defer  db.Close()
	stmt, err := db.Prepare("insert into chat_notifikasi value (?, ?, ?, ?)")
	checkErr(err)
	if err != nil {
		log.Println(err)
	}

	if res, err := stmt.Exec("null", chatNotifikasi.Id_dokter_pengirim, chatNotifikasi.Id_dokter_penerima, chatNotifikasi.Status); err != nil {
		checkErr(err)
		return 0
	} else {
		lastId, _ := res.LastInsertId()
		return int(lastId)
	}
	return 0

}



func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
