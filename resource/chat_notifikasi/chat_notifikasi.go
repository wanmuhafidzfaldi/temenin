package chat_notifikasi

type ChatNotifikasi struct{
	Id_chat_notifikasi  string `json:"idChatNotifikasi"`
	Id_dokter_pengirim  string `json:"idDokterPengirim"`
	Id_dokter_penerima  string `json:"idDokterPenerima"`
	Status				string `json:"status"`
}

