package rumah_sakit

type RumahSakit struct {
	Id_rs       string `json:"idRs"`
	No_reg_rs   string `json:"noRegRs"`
	Nama_rs     string `json:"namaRs"`
	Alamat_rs   string `json:"alamatRs"`
	Kab_kota_rs string `json:"kabKotaRs"`
	Provinsi_rs string `json:"provinsiRs"`
	Hp_rs       string `json:"hpRs"`
	Foto_rs     string `json:"fotoRs"`
	Peng_di_rs	string `json:"pengDiRs"`
}