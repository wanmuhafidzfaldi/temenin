package rumah_sakit

import "github.com/kataras/iris"

func ShowById(ctx iris.Context) {
	if (ctx.Params().Get("id_rs") != ""){
		type Response struct {
			Status int `json:"status"`
			Message string `json:"message"`
			RumahSakit RumahSakit `json:"rumahSakit"`
		}
		var response Response
		response.RumahSakit = GetById(ctx.Params().Get("id_rs"))
		if(response.RumahSakit.Id_rs == ""){
			response.Status = 0
			response.Message = "Error Data"
			ctx.JSON(response)
		} else {
			response.Status = 1
			response.Message = "Result Data"
			ctx.JSON(response)
		}
	}
}
