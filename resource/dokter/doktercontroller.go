package dokter

import (
	"github.com/kataras/iris"
)

func Show(ctx iris.Context)  {
	type Response struct {
		Status int `json:"status"`
		Message string `json:"message"`
		Dokter []Dokter `json:"dokter"`
	}
	var response Response
	response.Dokter = GetAll()
	if (len(response.Dokter) != 0){
		response.Status = 1
		response.Message = "Result Data"
		ctx.JSON(response)
	} else {
		response.Status = 0
		response.Message = "Error Data"
		ctx.JSON(response)
	}
}

func ShowById(ctx iris.Context){
	type Response struct {
		Status int `json:"status"`
		Message string `json:"message"`
		Dokter Dokter `json:"dokter"`
	}
	var response Response
	response.Dokter = GetById(ctx.Params().Get("id"))
	if(response.Dokter.Id_dokter == ""){
		response.Status = 0
		response.Message = "Error Data"
		ctx.JSON(response)
	} else {
		response.Status = 1
		response.Message = "Result Data"
		ctx.JSON(response)
	}
}

func ShowByName(ctx iris.Context){
	type Response struct {
		Status int `json:"status"`
		Message string `json:"message"`
		Dokter []Dokter `json:"dokter"`
	}
	var response Response
	response.Dokter = GetByName(ctx.Params().Get("nama_dokter"))
	if (len(response.Dokter) != 0){
		response.Status = 1
		response.Message = "Result Data"
		ctx.JSON(response)
	} else {
		response.Status = 0
		response.Message = "Error Data"
		ctx.JSON(response)
	}
}