package dokter

import (
	"log"
	"temenin/config/database"
)

func GetAll() []Dokter  {
	var dokter Dokter
	result := []Dokter{}
	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_dktr, id_rs, username_dktr, nama_dktr, spesialis_dktr, alamat_dktr, hp_dktr, tgl_lahir_dktr, foto_dktr, last_login_dktr, online, device_id, firebase_token from dokter")
	if err != nil {
		log.Print(err)
	}
	for rows.Next() {
		if err := rows.Scan(&dokter.Id_dokter, &dokter.Id_rs, &dokter.Username_dktr, &dokter.Nama_dktr, &dokter.Spesialis_dktr, &dokter.Alamat_dktr, &dokter.Hp_dktr, &dokter.Tgl_lahir_dokter ,&dokter.Foto_dktr, &dokter.Last_login_dktr, &dokter.Online, &dokter.Device_id, &dokter.Firebase_token); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {
			result = append(result, dokter)
		}
	}
	return result
}



func GetById(idDktr string) Dokter  {
	var dokter Dokter
	result := Dokter{}
	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_dktr, id_rs, username_dktr, nama_dktr, spesialis_dktr, alamat_dktr, hp_dktr, tgl_lahir_dktr, foto_dktr, last_login_dktr, online, device_id, firebase_token from dokter where id_dktr ='"+idDktr+"'")
	if err != nil {
		log.Print(err)
	}
	for rows.Next() {
		if err := rows.Scan(&dokter.Id_dokter, &dokter.Id_rs, &dokter.Username_dktr, &dokter.Nama_dktr, &dokter.Spesialis_dktr, &dokter.Alamat_dktr, &dokter.Hp_dktr, &dokter.Tgl_lahir_dokter ,&dokter.Foto_dktr, &dokter.Last_login_dktr, &dokter.Online, &dokter.Device_id, &dokter.Firebase_token); err != nil {
			log.Fatal(err.Error())
			return dokter
		} else {
			result = dokter
		}
	}
	return result
}

func GetByName(namaDokter string) []Dokter  {
	var dokter Dokter
	result := []Dokter{}
	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_dktr, id_rs, username_dktr, nama_dktr, spesialis_dktr, alamat_dktr, hp_dktr, tgl_lahir_dktr, foto_dktr, last_login_dktr, online, device_id, firebase_token from dokter where nama_dktr like '%"+namaDokter+"%'")
	if err != nil {
		log.Print(err)
	}
	for rows.Next() {
		if err := rows.Scan(&dokter.Id_dokter, &dokter.Id_rs, &dokter.Username_dktr, &dokter.Nama_dktr, &dokter.Spesialis_dktr, &dokter.Alamat_dktr, &dokter.Hp_dktr, &dokter.Tgl_lahir_dokter ,&dokter.Foto_dktr, &dokter.Last_login_dktr, &dokter.Online, &dokter.Device_id, &dokter.Firebase_token); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {
			result = append(result, dokter)
		}
	}
	return result
}
