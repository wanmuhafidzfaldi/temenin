package dokter

type Dokter struct {
	Id_dokter       	string `json:"idDokter"`
	Id_rs          	 	string `json:"idRs"`
	Username_dktr   	string `json:"usernameDktr"`
	Password_dktr   	string `json:"passwordDktr"`
	Nama_dktr       	string `json:"namaDktr"`
	Spesialis_dktr  	string `json:"spesialisDktr"`
	Alamat_dktr         string `json:"alamatDktr"`
	Hp_dktr         	string `json:"hpDktr"`
	Tgl_lahir_dokter 	string `json:"tglLahirDokter"`
	Foto_dktr       	string `json:"fotoDktr"`
	Last_login_dktr 	string `json:"lastLoginDktr"`
	Online          	string `json:"online"`
	Device_id       	string `json:"deviceId"`
	Firebase_token  	string `json:"firebaseToken"`
}

