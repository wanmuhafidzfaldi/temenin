package pasien

type Pasien struct {
	Id_pasien                string `json:"idPasien"`
	No_reg_pasien            string `json:"noRegPasien"`
	No_ktp_pasien            string `json:"noKtpPasien"`
	No_rekam_medis           string `json:"noRekamMedis"`
	Nama_pasien              string `json:"namaPasien"`
	Tgl_lahir_pasien         string `json:"tglLahirPasien"`
	Jk_pasien                string `json:"jkPasien"`
	Usia_pasien              string `json:"usiaPasien"`
	Alamat_pasien            string `json:"alamatPasien"`
	Menikah_pasien           string `json:"menikahPasien"`
	Pend_pasien              string `json:"pendPasien"`
	Pekerjaan                string `json:"pekerjaan"`
	Status_jaminan_pelayanan string `json:"statusJaminanPelayanan"`
	Foto_pasien              string `json:"fotoPasien"`
	Id_rs_diampu             string `json:"idRsDiampu"`
	Konsultasi               string `json:"konsultasi"`
}
