package pasien

import (
	"github.com/kataras/iris"
	"strings"
	"time"
	"strconv"
)

func Show(ctx iris.Context)  {
	type Response struct {
		Status int `json:"status"`
		Message string `json:"message"`
		Pasien []Pasien `json:"pasien"`
	}
	var response Response
	response.Pasien = GetAll()
	if (len(response.Pasien) != 0){
		response.Status = 1
		response.Message = "Result Data"
	} else {
		response.Status = 0
		response.Message = "Empty Data"
	}
	ctx.JSON(response)
}

func ShowById(ctx iris.Context){
	if (ctx.Params().Get("id") != ""){
		type Response struct {
			Status int `json:"status"`
			Message string `json:"message"`
			Pasien Pasien `json:"pasien"`
		}
		var response Response
		response.Pasien = GetById(ctx.Params().Get("id"))
		if(response.Pasien.Id_pasien == ""){
			response.Status = 0
			response.Message = "Error Data"
			ctx.JSON(response)
		} else {
			response.Status = 1
			response.Message = "Result Data"
			ctx.JSON(response)
		}
	} else {
		type Response struct {
			Status int `json:"status"`
			Message string `json:"message"`
			Pasien []Pasien `json:"pasien"`
		}
		var response Response
		if (ctx.Params().Get("id_rs") != "") {
			response.Pasien = GetByTable(ctx.Params().Get("id_rs"), "id_rs_diampu")
		} else if (ctx.Params().Get("nama_pasien") != "") {
			response.Pasien = GetByName(ctx.Params().Get("nama_pasien"), "nama_pasien")
		}
		if(len(response.Pasien) != 0){
			response.Status = 1
			response.Message = "Result Data"
			ctx.JSON(response)
		} else {
			response.Status = 0
			response.Message = "Error Data"
			ctx.JSON(response)
		}
	}


}
func Create(ctx iris.Context){
	type Response struct {
		Status int `json:"status"`
		Message string `json:"message"`
	}
	var response Response
	var pasien Pasien

	pasien.No_reg_pasien 			= ctx.FormValue("no_reg_pasien")
	pasien.No_ktp_pasien 			= ctx.FormValue("no_ktp_pasien")
	pasien.No_rekam_medis 			= ctx.FormValue("no_rekam_medis")
	pasien.Nama_pasien 				= strings.ToUpper(ctx.FormValue("nama_pasien"))
	pasien.Tgl_lahir_pasien 		= ctx.FormValue("tgl_lahir_pasien")
	pasien.Jk_pasien 				= strings.ToUpper(ctx.FormValue("jk_pasien"))
	if (pasien.Jk_pasien == "Laki - Laki") {
		pasien.Jk_pasien = "L"
	} else if (pasien.Jk_pasien == "Perempuan") {
		pasien.Jk_pasien = "P"
	}
	pasien.Usia_pasien              = ctx.FormValue("usia_pasien")
	pasien.Alamat_pasien            = strings.ToUpper(ctx.FormValue("alamat_pasien"))
	pasien.Menikah_pasien           = strings.ToUpper(ctx.FormValue("menikah_pasien"))
	pasien.Pend_pasien              = strings.ToUpper(ctx.FormValue("pend_pasien"))
	pasien.Pekerjaan                = strings.ToUpper(ctx.FormValue("pekerjaan"))
	pasien.Status_jaminan_pelayanan = strings.ToUpper(ctx.FormValue("status_jaminan_pelayanan"))
	pasien.Foto_pasien              = ctx.FormValue("foto_pasien")
	pasien.Id_rs_diampu             = ctx.FormValue("id_rs_diampu")
	pasien.Konsultasi 				= time.Now().Local().Format("2006-01-02 15:04:05")

	t,_ := time.Parse("02-01-2006", pasien.Tgl_lahir_pasien )

	pasien.Tgl_lahir_pasien = t.Format("2006-01-02")
	pasien.Usia_pasien = strconv.Itoa(time.Now().Year() - t.Year())+ " Tahun " + strconv.Itoa(int(time.Now().Month()) - int(t.Month())) + " Bulan"

	if (Insert(pasien) != 1){
		response.Status = 1
		response.Message = "Data Berhasil Diinput"
		ctx.JSON(response)
	} else {
		response.Status = 0
		response.Message = "Data Gagal Diinput"
		ctx.JSON(response)
	}
}