package pasien

import (
	"temenin/config/database"
	"log"
)

func GetAll() []Pasien  {
	var pasien Pasien
	result := []Pasien{}
	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_pasien, no_reg_pasien, no_ktp_pasien, no_rekam_medis, nama_pasien, date_format(tgl_lahir_pasien, '%d-%m-%Y'), jk_pasien, usia_pasien, alamat_pasien, menikah_pasien, pend_pasien, pekerjaan, status_jaminan_pelayanan, foto_pasien, id_rs_diampu, konsultasi from pasien")
	if err != nil {
		log.Print(err)
	}
	for rows.Next() {
		if err := rows.Scan(&pasien.Id_pasien, &pasien.No_reg_pasien, &pasien.No_ktp_pasien, &pasien.No_rekam_medis, &pasien.Nama_pasien, &pasien.Tgl_lahir_pasien, &pasien.Jk_pasien, &pasien.Usia_pasien, &pasien.Alamat_pasien, &pasien.Menikah_pasien, &pasien.Pend_pasien, &pasien.Pekerjaan, &pasien.Status_jaminan_pelayanan, &pasien.Foto_pasien, &pasien.Id_rs_diampu, &pasien.Konsultasi); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {
			result = append(result, pasien)
		}
	}
	return result
}

func GetById(idPasien string) Pasien{
	var pasien Pasien
	result := Pasien{}
	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_pasien, no_reg_pasien, no_ktp_pasien, no_rekam_medis, nama_pasien, date_format(tgl_lahir_pasien, '%d-%m-%Y'), jk_pasien, usia_pasien, alamat_pasien, menikah_pasien, pend_pasien, pekerjaan, status_jaminan_pelayanan, foto_pasien, id_rs_diampu, konsultasi from pasien where pasien.id_pasien = '"+idPasien+"'")
	if err != nil {
		log.Print(err)
	}
	for rows.Next() {
		if err := rows.Scan(&pasien.Id_pasien, &pasien.No_reg_pasien, &pasien.No_ktp_pasien, &pasien.No_rekam_medis, &pasien.Nama_pasien, &pasien.Tgl_lahir_pasien, &pasien.Jk_pasien, &pasien.Usia_pasien, &pasien.Alamat_pasien, &pasien.Menikah_pasien, &pasien.Pend_pasien, &pasien.Pekerjaan, &pasien.Status_jaminan_pelayanan, &pasien.Foto_pasien, &pasien.Id_rs_diampu, &pasien.Konsultasi); err != nil {
			log.Fatal(err.Error())
			return pasien
		} else {

			result = pasien
		}
	}
	return result
}

func GetByTable(value string, tableName string) []Pasien{
	var pasien Pasien
	result := []Pasien{}
	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_pasien, no_reg_pasien, no_ktp_pasien, no_rekam_medis, nama_pasien, date_format(tgl_lahir_pasien, '%d-%m-%Y'), jk_pasien, usia_pasien, alamat_pasien, menikah_pasien, pend_pasien, pekerjaan, status_jaminan_pelayanan, foto_pasien, id_rs_diampu, konsultasi from pasien where pasien."+tableName+" = '"+value+"'")
	if err != nil {
		log.Print(err)
	}
	for rows.Next() {
		if err := rows.Scan(&pasien.Id_pasien, &pasien.No_reg_pasien, &pasien.No_ktp_pasien, &pasien.No_rekam_medis, &pasien.Nama_pasien, &pasien.Tgl_lahir_pasien, &pasien.Jk_pasien, &pasien.Usia_pasien, &pasien.Alamat_pasien, &pasien.Menikah_pasien, &pasien.Pend_pasien, &pasien.Pekerjaan, &pasien.Status_jaminan_pelayanan, &pasien.Foto_pasien, &pasien.Id_rs_diampu, &pasien.Konsultasi); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {

			result = append(result, pasien)
		}
	}
	return result
}

func GetByName(value string, tableName string) []Pasien{
	var pasien Pasien
	result := []Pasien{}
	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_pasien, no_reg_pasien, no_ktp_pasien, no_rekam_medis, nama_pasien, date_format(tgl_lahir_pasien, '%d-%m-%Y'), jk_pasien, usia_pasien, alamat_pasien, menikah_pasien, pend_pasien, pekerjaan, status_jaminan_pelayanan, foto_pasien, id_rs_diampu, konsultasi from pasien where pasien."+tableName+" like '%"+value+"%'")
	if err != nil {
		log.Print(err)
	}
	for rows.Next() {
		if err := rows.Scan(&pasien.Id_pasien, &pasien.No_reg_pasien, &pasien.No_ktp_pasien, &pasien.No_rekam_medis, &pasien.Nama_pasien, &pasien.Tgl_lahir_pasien, &pasien.Jk_pasien, &pasien.Usia_pasien, &pasien.Alamat_pasien, &pasien.Menikah_pasien, &pasien.Pend_pasien, &pasien.Pekerjaan, &pasien.Status_jaminan_pelayanan, &pasien.Foto_pasien, &pasien.Id_rs_diampu, &pasien.Konsultasi); err != nil {
			log.Fatal(err.Error())
			return nil
		} else {

			result = append(result, pasien)
		}
	}
	return result
}

func Insert(pasien Pasien) int {
	db := database.ConnectMysqlDB()
	defer  db.Close()
	stmt, err := db.Prepare("insert into pasien value (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
	checkErr(err)
	if err != nil {
		log.Println(err)
	}
	if res, err := stmt.Exec("null", pasien.No_reg_pasien, pasien.No_ktp_pasien, pasien.No_rekam_medis, pasien.Nama_pasien, pasien.Tgl_lahir_pasien, pasien.Jk_pasien, pasien.Usia_pasien, pasien.Alamat_pasien, pasien.Menikah_pasien, pasien.Pend_pasien, pasien.Pekerjaan, pasien.Status_jaminan_pelayanan, pasien.Foto_pasien, pasien.Id_rs_diampu, pasien.Konsultasi); err != nil {
		checkErr(err)
		return 0
	} else {
		lastId, _ := res.LastInsertId()
		return int(lastId)
	}

	return 0;
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}