package tele

type TelePasien struct {
	Id_tele       string `json:"idTele"`
	Jenis_tele    string `json:"jenisTele"`
	Diagnosa      string `json:"diagnosa"`
	Tgl_foto      string `json:"tglFoto"`
	Ket_foto      string `json:"ketFoto"`
	Nadi          string `json:"nadi"`
	Nafas         string `json:"nafas"`
	Tekanan_darah string `json:"tekananDarah"`
	Suhu          string `json:"suhu"`
	Berat_badan   string `json:"beratBadan"`
	Kesadaran     string `json:"kesadaran"`
	Foto_tele     string `json:"fotoTele"`
	Status        string `json:"status"`
	Id_rs         string `json:"idRs"`
}



