package tele

import (
	"github.com/kataras/iris"
	"temenin/resource/pasien"
	"temenin/resource/riwayat_pasien"
	"fmt"
	"strings"
	"strconv"
	"time"
)

func Show(ctx iris.Context) {

}

func ShowById(ctx iris.Context) {
	type Response struct {
		Status        int                          `json:"status"`
		Message       string                       `json:"message"`
		Pasien        pasien.Pasien                `json:"pasien"`
		RiwayatPasien riwayat_pasien.RiwayatPasien `json:"riwayatPasien"`
		TelePasien    TelePasien                   `json:"telePasien"`
	}
	idTele := ctx.Params().Get("id_tele")
	var response Response
	response.RiwayatPasien = riwayat_pasien.GetById(idTele)
	response.Pasien = pasien.GetById(response.RiwayatPasien.Id_pasien)
	response.TelePasien = GetById(idTele)
	if (response.Pasien.Id_pasien == "") {
		response.Status = 0
		response.Message = "Error Data"
		ctx.JSON(response)
	} else {
		response.Status = 1
		response.Message = "Result Data"
		ctx.JSON(response)
	}

}

func Create(ctx iris.Context) {
	type Response struct {
		Status       int    `json:"status"`
		Message      string `json:"message"`
		Id_Rs_Pengampu string `json:"idRsPengampu"`
	}
	var response Response
	var tele TelePasien
	var riwayatPasien riwayat_pasien.RiwayatPasien

	tele.Jenis_tele = strings.ToUpper(ctx.FormValue("jenis_tele"))
	tele.Diagnosa = ctx.FormValue("diagnosa")
	tele.Tgl_foto = time.Now().Local().Format("2006-01-02 15:04:05")
	tele.Ket_foto = ctx.FormValue("ket_foto")
	tele.Nadi = ctx.FormValue("nadi")
	tele.Nafas = ctx.FormValue("nafas")
	tele.Tekanan_darah = ctx.FormValue("tekanan_darah")
	tele.Suhu = ctx.FormValue("suhu")
	tele.Berat_badan = ctx.FormValue("berat_badan")
	tele.Kesadaran = ctx.FormValue("Kesadaran")
	tele.Foto_tele = ctx.FormValue("foto_tele")
	tele.Status = "0rs"
	tele.Id_rs = ctx.FormValue("id_rs")
	idTele := Insert(tele)
	if (idTele != 0) {
		riwayatPasien.Id_tele = strconv.Itoa(idTele)
		riwayatPasien.Id_pasien = ctx.FormValue("id_pasien")
		riwayatPasien.Id_dokter = ctx.FormValue("id_dokter")
		riwayatPasien.Keluhan_utama = ctx.FormValue("keluhan_utama")
		riwayatPasien.Anamnesa_awal = ctx.FormValue("anamnesa_awal")
		riwayatPasien.Hasil_lab = ctx.FormValue("hasil_lab")
		riwayatPasien.Riwayat_alergi = ctx.FormValue("riwayat_alergi")
		riwayatPasien.Riwayat_penyakit = ctx.FormValue("riwayat_penyakit")
		riwayatPasien.Riwayat_pengobatan = ctx.FormValue("riwayat_pengobatan")
		if (riwayat_pasien.Insert(riwayatPasien) != 0) {
			response.Status = 1
			response.Message = "Data Berhasil Diinput"
			response.Id_Rs_Pengampu = GetById(strconv.Itoa(idTele)).Id_rs
			ctx.JSON(response)
		} else {
			response.Status = 0
			response.Message = "Data Gagal Diinput"
			ctx.JSON(response)
		}
	} else {
		response.Status = 0
		response.Message = "Data Gagal Diinput"
		ctx.JSON(response)
	}
}

func Update(ctx iris.Context) {
	type Response struct {
		Status  int    `json:"status"`
		Message string `json:"message"`
	}
	var response Response
	idTele := ctx.Params().Get("id_tele")
	fmt.Println(idTele)

	var riwayatPasien riwayat_pasien.RiwayatPasien
	var tele TelePasien
	riwayatPasien.Id_pasien = ctx.FormValue("id_pasien")
	riwayatPasien.Id_dokter = ctx.FormValue("id_dokter")
	riwayatPasien.Keluhan_utama = ctx.FormValue("keluhan_utama")
	riwayatPasien.Anamnesa_awal = ctx.FormValue("anamnesa_awal")
	riwayatPasien.Hasil_lab = ctx.FormValue("hasil_lab")
	riwayatPasien.Riwayat_alergi = ctx.FormValue("riwayat_alergi")
	riwayatPasien.Riwayat_penyakit = ctx.FormValue("riwayat_penyakit")
	riwayatPasien.Riwayat_pengobatan = ctx.FormValue("riwayat_pengobatan")

	tele.Jenis_tele = strings.ToUpper(ctx.FormValue("jenis_tele"))
	tele.Diagnosa = ctx.FormValue("diagnosa")
	tele.Tgl_foto = ctx.FormValue("tgl_foto")
	tele.Ket_foto = ctx.FormValue("ket_foto")
	tele.Nadi = ctx.FormValue("nadi")
	tele.Nafas = ctx.FormValue("nafas")
	tele.Tekanan_darah = ctx.FormValue("tekanan_darah")
	tele.Suhu = ctx.FormValue("suhu")
	tele.Berat_badan = ctx.FormValue("berat_badan")
	tele.Foto_tele = ctx.FormValue("foto_tele")
	tele.Status = ctx.FormValue("status")
	tele.Id_rs = ctx.FormValue("id_rs")

	if (riwayat_pasien.UpdateById(idTele, riwayatPasien) == 1 && UpdateById(idTele, tele) == 1) {
		response.Status = 1
		response.Message = "Data Berhasil Update"
		ctx.JSON(response)
	} else {
		response.Status = 0
		response.Message = "Data Gagal Update"
		ctx.JSON(response)
	}
}
