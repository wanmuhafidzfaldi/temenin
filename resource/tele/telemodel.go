package tele

import (
	"temenin/config/database"
	"log"
)

func GetById(idTele string) TelePasien  {
	var telePasien TelePasien
	result := TelePasien{}

	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_tele, jenis_tele, diagnosa, tgl_foto, ket_foto, nadi, nafas, tekanan_darah, suhu, berat_badan, kesadaran, foto_tele, status, id_rs from tele where id_tele = '"+idTele+"'")
	checkErr(err)
	for rows.Next() {
		if err := rows.Scan(&telePasien.Id_tele, &telePasien.Jenis_tele, &telePasien.Diagnosa, &telePasien.Tgl_foto, &telePasien.Ket_foto, &telePasien.Nadi, &telePasien.Nafas, &telePasien.Tekanan_darah, &telePasien.Suhu, &telePasien.Berat_badan, &telePasien.Kesadaran, &telePasien.Foto_tele, &telePasien.Status, &telePasien.Id_rs); err != nil {
			log.Fatal(err.Error())
			return telePasien
		} else {
			result = telePasien
		}
	}
	return result
}

func UpdateById(idTele string, tele TelePasien) int {
	db := database.ConnectMysqlDB()
	defer  db.Close()
	stmt, err := db.Prepare("update tele set jenis_tele = ?, diagnosa = ?, tgl_foto = ?, nadi = ?, nafas = ?, tekanan_darah = ?, suhu = ?, berat_badan = ?, kesadaran = ?, foto_tele = ?, status = ?, id_rs = ? where id_tele = ?")
	checkErr(err)
	if err != nil {
		log.Println(err)
	}

	if _, err := stmt.Exec(tele.Jenis_tele, tele.Diagnosa, tele.Tgl_foto, tele.Nadi, tele.Nafas, tele.Tekanan_darah, tele.Suhu, tele.Berat_badan, tele.Kesadaran, tele.Foto_tele, tele.Status, tele.Id_rs, idTele); err != nil {
		checkErr(err)
		return 0
	} else {
		return 1
	}
	return 0
}

func Insert(tele TelePasien) int {
	db := database.ConnectMysqlDB()
	defer  db.Close()
	var idRs string

	rows, err := db.Query("Select rumah_sakit.id_rs from rumah_sakit,rumah_sakit_koneksi where rumah_sakit.peng_di_rs = 'p2' and rumah_sakit.id_rs = rumah_sakit_koneksi.id_rs_koneksi and rumah_sakit_koneksi.id_rs ='" +tele.Id_rs+"'")
	checkErr(err)
	for rows.Next() {
		if err := rows.Scan(&idRs); err != nil {
			log.Fatal(err.Error())
		} else {
			idRs = idRs
		}
	}


	stmt, err := db.Prepare("insert into tele value (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
	checkErr(err)
	if err != nil {
		log.Println(err)
	}
	if res, err := stmt.Exec("null", tele.Jenis_tele, tele.Diagnosa, tele.Tgl_foto, tele.Ket_foto, tele.Nadi, tele.Nafas, tele.Tekanan_darah, tele.Suhu, tele.Berat_badan, tele.Kesadaran, tele.Foto_tele, tele.Status, idRs); err != nil {
		checkErr(err)
		return 0
	} else {
		lastId, _ := res.LastInsertId()
		return int(lastId)
	}
	return 0
}

func checkErr(err error) {
if err != nil {
panic(err)
}
}
