package riwayat_pasien

import (
	"temenin/config/database"
	"log"
	"fmt"
)

func GetById(idTele string) RiwayatPasien  {
	var riwayatPasien RiwayatPasien
	result := RiwayatPasien{}

	db := database.ConnectMysqlDB()
	defer db.Close()

	rows, err := db.Query("select id_riwayat, id_pasien, id_dokter, keluhan_utama, anamnesa_awal, hasil_lab, riwayat_alergi, riwayat_pengobatan, riwayat_penyakit, id_tele from riwayat_pasien where riwayat_pasien.id_tele = '"+idTele+"'")
	checkErr(err)
	for rows.Next() {
		if err := rows.Scan(&riwayatPasien.Id_riwayat, &riwayatPasien.Id_pasien, &riwayatPasien.Id_dokter, &riwayatPasien.Keluhan_utama, &riwayatPasien.Anamnesa_awal, &riwayatPasien.Hasil_lab, &riwayatPasien.Riwayat_alergi, &riwayatPasien.Riwayat_penyakit, &riwayatPasien.Riwayat_pengobatan, &riwayatPasien.Id_tele); err != nil {
			log.Fatal(err.Error())
			return riwayatPasien
		} else {
			result = riwayatPasien
		}
	}
	return result
}



func Insert(riwayatPasien RiwayatPasien) int{
	db := database.ConnectMysqlDB()
	defer  db.Close()
	fmt.Println(riwayatPasien.Id_tele)
	stmt, err := db.Prepare("insert into riwayat_pasien value (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
	checkErr(err)
	if err != nil {
		log.Println(err)
	}

	if res, err := stmt.Exec("null", riwayatPasien.Id_pasien, riwayatPasien.Id_dokter, riwayatPasien.Keluhan_utama, riwayatPasien.Anamnesa_awal, riwayatPasien.Hasil_lab, riwayatPasien.Riwayat_alergi, riwayatPasien.Riwayat_penyakit, riwayatPasien.Riwayat_pengobatan, riwayatPasien.Id_tele); err != nil {
		checkErr(err)
		return 0
	} else {
		lastId, _ := res.LastInsertId()
		return int(lastId)
	}
	return 0

}

func UpdateById(idTele string, riwayatPasien RiwayatPasien) int{
	db := database.ConnectMysqlDB()
	defer  db.Close()
	stmt, err := db.Prepare("update riwayat_pasien set id_pasien = ?, id_dokter = ?, keluhan_utama = ?, anamnesa_awal = ?, hasil_lab = ?, riwayat_alergi = ?, riwayat_penyakit = ?, riwayat_pengobatan = ? where id_tele = ?")
	checkErr(err)
	if err != nil {
		log.Println(err)
	}

	if _, err := stmt.Exec(riwayatPasien.Id_pasien, riwayatPasien.Id_dokter, riwayatPasien.Keluhan_utama, riwayatPasien.Anamnesa_awal, riwayatPasien.Hasil_lab, riwayatPasien.Riwayat_alergi, riwayatPasien.Riwayat_penyakit, riwayatPasien.Riwayat_pengobatan, idTele); err != nil {
		checkErr(err)
		return 0
	} else {
		return 1
	}
	return 0

}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}