package riwayat_pasien

type RiwayatPasien struct {
	Id_riwayat          string `json:"idRiwayat"`
	Id_pasien           string `json:"idPasien"`
	Id_dokter           string `json:"idDokter"`
	Keluhan_utama       string `json:"keluhanUtama"`
	Anamnesa_awal       string `json:"anamnesaAwal"`
	Hasil_lab           string `json:"hasilLab"`
	Riwayat_alergi      string `json:"riwayatAlergi"`
	Riwayat_penyakit    string `json:"riwayatPenyakit"`
	Riwayat_pengobatan  string `json:"riwayatPengobatan"`
	Id_tele				string `json:"idTele"`
}

