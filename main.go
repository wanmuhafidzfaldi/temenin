package main

import (
	"temenin/config"
	"github.com/kataras/iris"
)

func main() {
	app := config.Router()


	app.Run(iris.Addr(":8080"))
}
